import collections
import os
infile=os.path.join(os.path.dirname(__file__), 'part1-input.txt')

def char_diff(a, b):
  diff_count = 0
  diff_positions = []
  if len(a)!=len(b):
    raise Exception('cannot chardiff strings of unequal length')
  for i in range(0,len(a)):
    if a[i] != b[i]:
      diff_count += 1
      diff_positions.append(i)
  return diff_count, diff_positions


def main():
  with open(infile, 'rb') as f:
    lines = f.readlines()
  lines = [l.strip() for l in lines if len(l.strip())>0]
  lines = sorted(lines)

  for i in range(0, len(lines)-1):
    dc, dp = char_diff(lines[i], lines[i+1])
    if dc==1:
      print('@%d: %s' % (i, lines[i]))
      print('@%d: %s' % (i+1, lines[i+1]))
      common_chars = ''.join([chr(c) for pos, c in enumerate(lines[i]) if not pos in dp])
      print('common chars: %s'% common_chars)

if __name__ == '__main__':
  main()