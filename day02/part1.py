import collections
import os
infile=os.path.join(os.path.dirname(__file__), 'part1-input.txt')

def count23(s):
  has2 = 0
  has3 = 0
  ccounts = collections.defaultdict(int)
  for c in s:
    ccounts[c] += 1
  for c, count in ccounts.items():
    if count==2: has2 = 1
    if count==3: has3 = 1
  return has2, has3

def main():
  with open(infile, 'rb') as f:
    lines = f.readlines()
  lines = [l.strip() for l in lines if len(l.strip())>0]
  counts = [count23(s) for s in lines] # [(0,0),(1,0),(1,1),(0,1),...]
  num2 = sum([x[0] for x in counts])
  num3 = sum([x[1] for x in counts])
  print('checksum: %d' % (num2*num3))

if __name__ == '__main__':
  main()