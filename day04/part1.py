import os
import re
import collections
infile=os.path.join(os.path.dirname(__file__), 'part1-input.txt')

re_guard = re.compile('^\[[0-9- :]+\] Guard #([0-9]+) begins shift$')
re_sleep = re.compile('^\[[0-9-]+ ..:(..)\] falls asleep$')
re_awake = re.compile('^\[[0-9-]+ ..:(..)\] wakes up$')
# [1518-04-15 00:51] wakes up
# [1518-07-15 00:53] falls asleep

def main():
  with open(infile, 'rb') as f:
    lines = f.readlines()
  lines = [l.strip().decode('utf8') for l in lines if len(l.strip())>0]

  # all lines have the format
  #   [1518-10-14 00:30] <description>
  # so sorting sorts by timestamp
  lines = sorted(lines)

  # for l in lines:
  #   print(l)
  sleep_count_by_guard_minute = {}
  active_guard = None
  for l in lines:
    m_guard = re_guard.match(l)
    m_sleep = re_sleep.match(l)
    m_awake = re_awake.match(l)
    if m_guard:
      active_guard = m_guard.group(1)
      sleep_minute = None
      awake_minute = None
    elif m_sleep:
      sleep_minute = int(m_sleep.group(1))
    elif m_awake:
      awake_minute = int(m_awake.group(1))
      # insert into sleep_count_by_guard_minute
      if active_guard not in sleep_count_by_guard_minute:
        sleep_count_by_guard_minute[active_guard] = collections.defaultdict(int)
      for m in range(sleep_minute, awake_minute):
        sleep_count_by_guard_minute[active_guard][m] += 1
    else:
      raise Exception('could not match line: %s' % l)
  
  # print(str(sleep_minutes_by_guard))

  max_guard = None
  max_guard_minutes = 0
  for g, by_min in sleep_count_by_guard_minute.items():
    total_minutes = 0
    for m, cnt in by_min.items():
      total_minutes += cnt
    if total_minutes > max_guard_minutes:
      max_guard = g
      max_guard_minutes = total_minutes
  print('sleepiest guard %s', max_guard)
  gm = sleep_count_by_guard_minute[max_guard]
  cm_tuples = list(gm.items())
  count_min_sorted = sorted(
    [(x,y) for (y,x) in cm_tuples]
    )
  print('%d, %d' % count_min_sorted[-1])
  
  # sleepiest guard %s 761
  # 12, 25
  # ans 19025

if __name__ == '__main__':
  main()