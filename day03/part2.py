import os
import re
infile=os.path.join(os.path.dirname(__file__), 'part1-input.txt')

class Claim(object):
  # each line is of this format:
  #     #1 @ 12,548: 19x10
  claim_re = re.compile('^#([0-9]+) @ ([0-9]+),([0-9]+): ([0-9]+)x([0-9]+)$')
  def __init__(self, claimstr):
    self.claimstr = claimstr
    self.parse_claim_str(self.claimstr)

  def parse_claim_str(self, claimstr):
    m = self.claim_re.match(claimstr)
    self.claim_id = int(m.group(1))
    self.x = int(m.group(2))
    self.y = int(m.group(3))
    self.width = int(m.group(4))
    self.height = int(m.group(5))

  def __str__(self):
    return 'Claim(id=%d, x=%d, y=%d, w=%d, h=%d)' % (self.claim_id, self.x, self.y, self.width, self.height)

  def coords_in_claim(self, x, y):
    return ((x >= self.x and x < self.x+self.width) and
            (y >=self.y and y < self.y+self.height))

  def __hash__(self):
    return hash(self.claim_id)

  def __eq__(self, other):
    return self.claim_id==other.claim_id


def main():
  with open(infile, 'rb') as f:
    lines = f.readlines()
  lines = [l.strip().decode('utf8') for l in lines if len(l.strip())>0]
  claims = set([Claim(l) for l in lines])
  nonoverlap_claims = claims.copy()

  max_x = max([c.x + c.width for c in claims])
  max_y = max([c.y + c.height for c in claims])

  print('max_x=%d, max_y=%d' % (max_x, max_y))

  for x in range(0, max_x):
    print('doing col %d of %d' % (x, max_x))
    for y in range(0, max_y):
      claims_here = set([c for c in claims if c.coords_in_claim(x,y)])
      if len(claims_here) > 1:
        for c in claims_here:
          nonoverlap_claims.discard(c)
  print('found %d non-overlapping claims:' % len(nonoverlap_claims))
  for c in nonoverlap_claims:
    print('  %s'% c)

  # found 1 non-overlapping claims:
  #   Claim(id=919, x=470, y=409, w=27, h=23)

if __name__ == '__main__':
  main()