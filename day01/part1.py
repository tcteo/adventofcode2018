import os
infile=os.path.join(os.path.dirname(__file__), 'part1-input.txt')

def main():
  with open(infile, 'rb') as f:
    lines = f.readlines()
  lines = [l.strip() for l in lines if len(l.strip())>0]
  print('read %d lines' % len(lines))
  n = [int(s) for s in lines]
  result = sum(n)
  print('result: %d' % result)

if __name__ == '__main__':
  main()