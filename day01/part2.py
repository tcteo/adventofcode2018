import os
infile=os.path.join(os.path.dirname(__file__), 'part2-input.txt')

def main():
  with open(infile, 'rb') as f:
    lines = f.readlines()
  lines = [l.strip() for l in lines if len(l.strip())>0]
  print('read %d lines' % len(lines))
  numbers = [int(s) for s in lines]

  seen_freq = set()
  freq = 0
  i=0
  while True:
    freq = freq + numbers[i % len(numbers)]
    if freq in seen_freq:
      print('%d has been seen at i=%d' % (freq, i))
      break
    seen_freq.add(freq)
    i += 1

if __name__ == '__main__':
  main()