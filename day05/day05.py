#!/usr/bin/env python
# coding: utf-8

# # part 1
# 

# In[54]:


import os


# In[55]:


os.getcwd()


# In[56]:


infile = os.path.join(os.getcwd(), 'day05-input.txt')
os.stat(infile)


# In[57]:


with open(infile,'rb') as f:
  lines = f.readlines()
print(len(lines))


# In[64]:


s_bytes = lines[0].strip()


# In[65]:


print(s_bytes[0:10])
print(len(s_bytes))


# ## parse

# In[60]:


def react(s):
  i=0
  while True:
    if i > len(s)-2 or i<0:
      break
    c1=s[i]
    c2=s[i+1]
    if ((c1.islower() and c2.isupper()) or (c1.isupper() and c2.islower())) and c1.lower()==c2.lower():
      # react
      s = s[0:i]+s[i+2:]
      i-=1
    i+=1
  return s


# In[61]:


print(react('aA'))
print(react('AaBb'))
print(react('AaBbC'))
print(react('AaCBb'))


# In[72]:


s = s_bytes.decode('utf8')

def react_fully(s):
  slen = len(s)
  while True:
    s = react(s)
    newlen = len(s)
    if slen==newlen:
      break
    slen = newlen
  return s

reacted_s = react_fully(s)

print('final len %d' % len(reacted_s))


# In[ ]:





# # part 2

# In[82]:


def remove_char(s, c):
  # remove all instances of char c from string s
  # remove both lowr and upper case 
  t = s.replace(c.lower(), '')
  t = t.replace(c.upper(), '')
  return t


# In[84]:


remove_char('hellO world', 'o')


# In[76]:


def unique_chars(s):
  # find unique chars in s
  # only return lower case chars
  chars = set()
  for c in s:
    chars.add(c.lower())
  return chars


# In[78]:


unique_chars('heLlo world')


# In[79]:


charz = unique_chars(s)
print(len(charz))


# In[85]:


n = 0
shortest_len = None
removed_char = None
for c in charz:
  n += 1
  print('%d of %d' % (n, len(charz)))
  s_removed = remove_char(s, c)
  s_reacted = react_fully(s_removed)
  reacted_len = len(s_reacted)
  if shortest_len==None or reacted_len < shortest_len:
    removed_char = c
    shortest_len = reacted_len
print('shortest len %d by removing char %s' % (shortest_len, removed_char))


# In[86]:


# shortest len 12908 by removing char j: is wrong. too high
# bug: wasn't removing both upper/lower. eg. was only removing 'a' and not 'A' in each try


# In[ ]:




