#!/bin/bash
set -e 
DIR="$( cd "$( readlink -f $(dirname "${BASH_SOURCE[0]}" ))" && pwd )"
cd "${DIR}"

ve_dir="ve"

if [ ! -d "${ve_dir}" ]; then
  virtualenv -p python3 "${ve_dir}"
fi

"${ve_dir}/bin/pip" install -r requirements.txt

"${ve_dir}/bin/jupyter-notebook" --notebook-dir "${DIR}" --no-browser

